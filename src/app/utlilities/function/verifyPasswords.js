//función para ahorrar codigo [document.getElementById() = __()];
function __(id){
  return document.getElementById(id);
}

//Validar contraseña
function validarContraseña() {
  var password1 = __('password1').value,
  password2 = __('password2').value;
  if(password1 != '' && password2 != ''){
    if(password1 != password2){
      //si las contraseñas no coinciden
      __('resultado').innerHTML = '<p class="error"><strong>Error: </strong>¡Las contraseñas no coinciden!</p>';
    } else {
      //Si todo esta correcto 
      __('form').innerHTML = '<p class="correcto"><strong>✓ Correcto: </strong>Los datos coinciden</p>';
    }
  } else {
    //si los campos o uno, este vacio
    __('resultado').innerHTML = '<p class="error"><strong>Error: </strong>¡Los campos no deben estar vacios!</p>';
  } 
}

//enviar formulario con la tecla ENTER
function enterEnviar(event){
    if(event.keyCode == 13){
      validarContraseña()
    }
}
