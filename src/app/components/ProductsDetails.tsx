import React, { useState } from "react";
import "pure-react-carousel/dist/react-carousel.es.css";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  ImageWithZoom,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import products4 from "../../assets/img/products/products4.jpg";
import products5 from "../../assets/img/products/products5.jpg";
import products6 from "../../assets/img/products/products6.jpg";

export const ProductsDetails = () => {

  const [cantidad, setCantidad] = useState(1)

  const editarCantidad = (accion:string) => {
    if(accion === "sumar") {
      setCantidad(cantidad + 1)
    }else if(accion === "restar" && cantidad != 1){
      setCantidad(cantidad - 1)
    }
  }
  return (
    <>
      <div className="products-details-area ptb-100">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-5 col-md-12">
              <CarouselProvider
                naturalSlideWidth={150}
                naturalSlideHeight={150}
                totalSlides={3}
                hasMasterSpinner
              >
                <Slider>
                  <Slide index={0}>
                    <ImageWithZoom src={products4} alt="producto4" />
                  </Slide>
                  <Slide index={1}>
                    <ImageWithZoom src={products5} alt="producto5" />
                  </Slide>
                  <Slide index={2}>
                    <ImageWithZoom src={products6} alt="producto6" />
                  </Slide>
                </Slider>
                <ButtonBack className="btn btn-dark mt-2">Anterior</ButtonBack>
                <ButtonNext className="btn btn-dark float-end mt-2">Siguiente</ButtonNext>
              </CarouselProvider>
            </div>
            <div className="col-lg-7 col-md-12">
              <div className="products-details-desc">
                <h3>Stack pet collars</h3>
                <div className="price">
                  <span className="new-price">$35.00</span>
                  <span className="old-price">$55.00</span>
                </div>

                <p>
                  Santiago who travels from his homeland in Spain to the
                  Egyptian desert in search of a treasure buried near the
                  Pyramids. Lorem ipsum dolor sit amet, consectetur elit, sed do
                  eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit
                  amet, consectetur adipiscing elitet.
                </p>
                <div className="products-add-to-cart">
                  <div className="input-counter">
                    <span className="minus-btn" onClick={() => editarCantidad("restar")}>
                      <i className="bx bx-minus"></i>
                    </span>
                    <input type="text" value={cantidad} />
                    <span className="plus-btn" onClick={() => editarCantidad("sumar")}>
                      <i className="bx bx-plus"></i>
                    </span>
                  </div>
                  <button type="submit" className="default-btn">
                    <span>Agregar al carrito</span>
                  </button>
                </div>
                <a href="wishlist.html" className="add-to-wishlist">
                  <i className="bx bx-heart"></i> Add to wishlist
                </a>
                <ul className="products-info">
                  <li>
                    <span>Codigo:</span> 007
                  </li>
                  <li>
                    <span>Categoria:</span> Brash
                  </li>
                  <li>
                    <span>Disponibles:</span> 7 items
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-12 col-md-12">
              <div className="products-details-tabs">
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item">
                    <button
                      className="nav-link active"
                      id="description-tab"
                      data-bs-toggle="tab"
                      data-bs-target="#description"
                      type="button"
                      role="tab"
                      aria-controls="description"
                      aria-selected="false"
                    >
                      Description
                    </button>
                  </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                  <div
                    className="tab-pane fade show active"
                    id="description"
                    role="tabpanel"
                  >
                    <p>
                      This story, dazzling in its powerful simplicity and
                      soul-stirring wisdom, is about an Andalusian shepherd boy
                      named Santiago who travels from his homeland in Spain to
                      the Egyptian desert in search of a treasure buried near
                      the Pyramids. Lorem ipsum dolor sit.
                    </p>
                    <ul>
                      <li>
                        Instant <strong>Patoi</strong> bestseller
                      </li>
                      <li>Translated into 18 languages</li>
                      <li>#1 Most Recommended Book of the year.</li>
                      <li>
                        A neglected project, widely dismissed, its champion
                        written off as unhinged.
                      </li>
                      <li>
                        Yields a negative result in an experiment because of a
                        flaw in the design of the experiment.
                      </li>
                      <li>
                        An Amazon, Bloomberg, Financial Times, Forbes, Inc.,
                        Newsweek, Strategy + Business, Tech Crunch, Washington
                        Post Best Business Book of the year
                      </li>
                    </ul>
                    <p>
                      <i>
                        This story, dazzling in its powerful simplicity and
                        soul-stirring wisdom, is about an Andalusian shepherd
                        boy named Santiago who travels from his homeland in
                        Spain to the Egyptian desert in search of a treasure
                        buried near the Pyramids. Lorem ipsum dolor sit.
                      </i>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
