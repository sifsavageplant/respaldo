import React from "react";

import about1 from "../../assets/img/about/about1.jpg";
import about2 from "../../assets/img/about/about2.jpg";


export const AboutUs = () => {
  return (
<div  className="about-area pt-100 pb-75">
            <div  className="container">
                <div  className="row align-items-center">
                    <div  className="col-lg-3 col-md-12">
                        <div  className="about-image">
                            <img src={about1} alt="about-us-nice-pict1"/>
                        </div>
                    </div>
                    <div  className="col-lg-6 col-md-12">
                        <div  className="about-text">
                            <h2>kimer Shop</h2>
                            <span>En Kimer shop Alegramos corazones porque tu felicidad y la de tu mascota hacen parte de nuestra Alegría ¡Unete a nuestra familia Kimer Shop!</span>
                            <p>Ofrecemos productos de calidad para tus mascotas .Nuestra misión hacerte feliz a ti y a tus amigos peludos con los mejores productos del mercado, llevamos más de 5 años en el mercado y sabemos cómo ofrecerte un buen servicio. Nuestros clientes nos aprecian y confían en nosotros por nuestro servicio y calidad ¿Ya compraste? No esperes más Bienvenido Kimer Shop.</p>
                        </div>
                    </div>
                    <div  className="col-lg-3 col-md-12">
                        <div  className="about-image">
                            <img src={about2} alt="about-us-nice-pict2"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
};
