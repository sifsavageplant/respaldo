import React from "react";
import { Link } from "react-router-dom";

export const BannerHome = () => {
  return(
<div className="main-banner-area">
            <div className="container-fluid">
                <div className="row align-items-center">
                    <div className="col-lg-6 col-md-12">
                        <div className="main-banner-content mt-4">
                            <span className="sub-title ">Bienvenido a la tienda de mascotas Kimer </span>
                            <h1> los mejores productos para mascotas</h1>
                            <p>Ahorra hasta el 20% de tu compra en tu primer pedido </p>
                            <Link 
                                to="#" 
                                className="default-btn mb-4">
                                    <span>Comprar ahora</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  );
};
