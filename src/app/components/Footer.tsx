import React from 'react'
import { Link } from 'react-router-dom'

import whiteLogo from "../../assets/img/white-logo.png"

export const Footer = () => {
  return (
    <>
    <footer className="footer-area">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <a href="index.html" className="logo">
                                <img src={whiteLogo} alt="logo"/>
                            </a>
                            <ul className="footer-contact-info">
                               
                                <li><span>Tech support:</span> <a href="tel:+1514312-5678">+1 (514) 312-000-000</a></li>
                                <li><span>Email:</span> <a href="mailto:hello@patoi.com">hello@kimer.com</a></li>

                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <div className="single-footer-widget pl-4">
                            <h3>Information</h3>
                            <ul className="custom-links">
                                <li><a href="about.html">About Us</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <h3>Customer service</h3>
                            <ul className="custom-links">
                                <li><Link to="/myaccount">My Account</Link></li>
                               
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="terms-conditions.html">Delivery Information</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <h3>Subscribe to our newsletter!</h3>
                            <p>Sign up for our mailing list to get the latest updates news & offers.</p>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
            <div className="copyright-area">
                <div className="container">
                    <p>Copyright @2022 <span>Kimer</span>. Design & Developed by Kimer</p>
                </div>
            </div>
        </footer>
    </>
  )
}
