import React from "react";
import about1 from "../../assets/img/about/about1.jpg";
import about2 from "../../assets/img/about/about2.jpg";

export const AboutArea = () => {
  return (
    <>
      <div className="about-area pt-100 pb-75">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-3 col-md-12">
              <div className="about-image">
                <img src={about1} alt="about us" />
              </div>
            </div>
            <div className="col-lg-6 col-md-12">
              <div className="about-text">
                <h2>The Story of Our Journey</h2>
                <span>
                  Cras ultricies ligula sed magna dictum porta. Curabitur non
                  nulla sit amet nisl tempus convallis quis ac lectus. Cras
                  ultricies ligula sed magna dictum porta.
                </span>
                <p>
                  We offer quality products at low prices every day. We have
                  been in this business for most 20 years. We have been doing
                  online shopping very confiden tly. Our customers like and
                  trust us so much because of the quality of our site's
                  products. Cras ultricies ligula sed magna dictum porta low
                  prices every day.
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-12">
              <div className="about-image">
                <img src={about2} alt="about us 2" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
