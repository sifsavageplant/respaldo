import React from 'react'
import { ProductsDetails } from '../../components/ProductsDetails'
import { TitleArea } from '../../components/TitleArea'
export const ProductsDetailsScreen = () => {
  return (
    <>
        <TitleArea title="Detalles de producto" />
        <ProductsDetails/>
    </>
  )
}

