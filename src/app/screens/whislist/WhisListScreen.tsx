import React from 'react'
import { TitleArea } from '../../components/TitleArea'
import WishList from '../../components/WishList'

export const WhisListScreen = () => {
  return(
     <>
  <TitleArea 
    title = "Lista de deseos"/>
    <WishList />
     </>);
};