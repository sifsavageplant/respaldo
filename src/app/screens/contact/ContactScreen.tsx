import React from "react";
import { ContactUs } from "../../components/ContactUs";
import { TitleArea } from "../../components/TitleArea";

export const ContactScreen = () => {
	return (
		<>
			<TitleArea title="Contactanos" />
			<ContactUs />
		</>
	);
};
