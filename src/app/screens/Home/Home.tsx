import React from 'react'

import { AboutUs } from '../../components/AboutUs'
import { BannerHome } from '../../components/BannerHome'

export const Home = () => {
  return (
    <>
      <BannerHome/>
      <AboutUs/>
    </>
  );
}
